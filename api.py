# importing the flask library files
from flask import Flask
from flask import abort

from .pdf import PDF

#create flask web app object
app = Flask(__name__)

# define http route
@app.route("/split-pdf")
def index():
    pdf = PDF('/tmp/split_pdf/')
    try:
        pdf.split()
        return 200
    except Exception as e:
        return abort(500)

#run the app
if __name__ == "__main__":
    app.run()
