#from PyPDF2 import PdfFileWriter, PdfFileReader


class PDF:
    def __init__(self, pathname):
        self.pathname = pathname

    def _get_pdf(self):
        return False

    def _clean_filename(self):
        return False

    def _create_folder(self):
        self._clean_filename()
        return False

    def _compress_folder(self):
        return False

    def _split_pdf(self):
        #input_pdf = PdfFileReader("input-file.pdf")
        #output = PdfFileWriter()
        #output.addPage(input_pdf.getPage(0))

        #with open("first_page.pdf", "wb") as output_stream:
        #    output.write(output_stream)

        return False

    def _cleanup(self):
        return False

    def split(self):
        self._get_pdf()
        self._create_folder()
        self._split_pdf()
        self._compress_folder()
        self._cleanup()
